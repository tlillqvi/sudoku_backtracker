#include <string>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <iomanip>
#include <fstream>
#include <functional>

using namespace std;

vector<int> load_puzzle(string filename)
{
    ifstream puzzle_file(filename.c_str());
    if( not puzzle_file.good() ) throw "Failed to open puzzle file";

    istream_iterator<char> eof_it;
    istream_iterator<char> puzzle_file_it(puzzle_file);

    vector<char> char_puzzle_vector;

    // copy chars between '0' and '9' from input file
    auto zero_to_nine = [](decltype(char_puzzle_vector)::value_type c ) { return '0' <= c && c <= '9'; };
    copy_if( puzzle_file_it, eof_it, back_inserter(char_puzzle_vector), zero_to_nine );

    if( char_puzzle_vector.size() < 81 ) throw "Not enough numbers in puzzle file. Expected 9x9";

    // convert puzzle from vector<char> to vector<int>
    vector<int> puzzle_vector;
    auto ctoi = [](decltype(char_puzzle_vector)::value_type c ) { return c - '0'; };
    transform( char_puzzle_vector.begin(), char_puzzle_vector.end(), back_inserter(puzzle_vector), ctoi );

    return puzzle_vector;

    //vector<int> puzzle(81,0);
    vector<int> puzzle = {0,5,4, 0,7,0, 0,0,0,
                 0,0,0, 1,8,0, 0,0,0,
                 0,0,6, 0,0,5, 0,3,9,

                 0,0,0, 0,1,0, 0,2,0,
                 0,3,0, 9,0,2, 0,0,0,
                 0,7,0, 0,0,0, 6,0,4,

                 0,2,0, 3,5,0, 0,0,0,
                 0,0,0, 0,0,0, 0,0,0,
                 7,0,0, 0,6,0, 0,8,0};
    return puzzle;
}

bool print_puzzle( vector<int> puzzle )
{
    cout << setfill('-') << setw(9+8) << "-" << endl;
    for( int r = 0; r < 9; r++)
    {
        ostream_iterator<int> out_it(cout,",");
        copy( puzzle.begin() + r*9, puzzle.begin() + r*9+8, out_it);
        cout << puzzle[r*9+8] << endl;
    }
    cout << setfill('-') << setw(9+8) << "-" << endl;
    return false; // When used as a visitor, the return value tells solve if it should stop or not.
}

bool row_ok( const vector<int>& puzzle, int cell )
{
    int row = (cell/9);
    for( int i = row*9; i < (row*9+9); i++ ) {
        if( puzzle[i] == 0 || cell == i ) continue;
        if( puzzle[cell] == puzzle[i] ) {
            //cout << "row fail cell:"<<cell<<" == i:"<<i << endl;
            return false;
        }
    }
    return true;
}

bool col_ok( const vector<int>& puzzle, int cell )
{
    for( int i = cell%9; i < 81; i+=9 ) {
        if( puzzle[i] == 0 || cell == i ) continue;
        if( puzzle[cell] == puzzle[i] ) {
            //cout << "col fail cell:"<<cell<<" == i:"<<i << endl;
            return false;
        }
    }
    return true;
}

bool square_ok( const vector<int>& puzzle, int cell )
{
    int col_start = cell%9-cell%3;
    int row_start = (cell/27)*3;
    //cout << "Cell: " << cell << " col_start = " << col_start << " row_start = " << row_start << endl;
    for( int r = row_start; r < row_start+3; r++ ) {
        for( int c = col_start; c < col_start+3; c++ ) {
            int i = 9*r+c;
            if( puzzle[i] == 0 || cell == i ) {
                //if(cell == 15) cout << "i = " << i << " OK cont" << endl;
                continue;
            }
            if( puzzle[cell] == puzzle[i] ) return false;
            //if(cell == 15) cout << "i = " << i << " OK" << endl;
        }
    }
    //if(cell == 15) print_puzzle( puzzle );
    return true;
}

bool invariant(const vector<int>& puzzle, int cell)
{
    if( ! row_ok( puzzle, cell ) ) { return false; }
    if( ! col_ok( puzzle, cell ) ) { return false; }
    if( ! square_ok( puzzle, cell ) ) { return false; }

    return true;
}

bool solve(vector<int>& puzzle, std::function<bool (vector<int>&)> visitor, int cell = 0)
{
    if( cell == 81 ) { return visitor( puzzle ); } // if visitor returns true then the solving stops, otherwise it continues.

    if( puzzle[cell] == 0 ) {
        for(int i = 1; i < 10; i++)
        {
            puzzle[cell] = i;
            if( invariant( puzzle, cell ) )
            {
                //cout << "fixit1 cell: " << cell+1 << endl;
                if( solve( puzzle, visitor, cell+1 ) ) {
                    return true;
                }
            }
        }
        puzzle[cell] = 0;
    } else {
        //cout << "fixit2 cell: " << cell+1 << endl;
        if( solve( puzzle, visitor, cell+1 ) ) {
            return true;
        }        
    }
    //cout << "fixit fail at cell: " << cell << endl;
    //sleep(1);
    return false; // No solution found down this path;
}

void print_usage(string binname)
{
        cout << "Usage: " << binname << " <puzzlefile>" << endl;
        cout << "Alternative for bash users: " << binname << R"MEH( <(echo "
 0,5,4, 0,7,0, 0,0,0,
 0,0,0, 1,8,0, 0,0,0,
 0,0,6, 0,0,5, 0,3,9,

 0,0,0, 0,1,0, 0,2,0,
 0,3,0, 9,0,2, 0,0,0,
 0,7,0, 0,0,0, 6,0,4,

 0,2,0, 3,5,0, 0,0,0,
 0,0,0, 0,0,0, 0,0,0,
 7,0,0, 0,6,0, 0,8,0"))MEH" << endl;
}

int self_test()
{
    vector<int> puzzle = {0,5,4, 0,7,0, 0,0,0,
                          0,0,0, 1,8,0, 0,0,0,
                          0,0,6, 0,0,5, 0,3,9,

                          0,0,0, 0,1,0, 0,2,0,
                          0,3,0, 9,0,2, 0,0,0,
                          0,7,0, 0,0,0, 6,0,4,

                          0,2,0, 3,5,0, 0,0,0,
                          0,0,0, 0,0,0, 0,0,0,
                          7,0,0, 0,6,0, 0,8,0};

    vector<int> expected = {3,5,4, 6,7,9, 2,1,8,
                            2,9,7, 1,8,3, 4,5,6,
                            8,1,6, 4,2,5, 7,3,9,

                            4,8,9, 7,1,6, 5,2,3,
                            6,3,5, 9,4,2, 8,7,1,
                            1,7,2, 5,3,8, 6,9,4,

                            9,2,8, 3,5,4, 1,6,7,
                            5,6,1, 8,9,7, 3,4,2,
                            7,4,3, 2,6,1, 9,8,5};

    auto verify = [expected](decltype(expected) result) { if(expected != result) throw "self test failed!"; return true; };
    solve( puzzle, verify );
    return 0;
}

int main(int argc, char* argv[])
{
    if( argc < 2 )
    {
        cerr << "ERROR: Not enough paramters\n" << endl;
        print_usage(argv[0]);
        return 1;
    }

    string matching_what;
    auto matching = [&matching_what](const char* str) { return matching_what == str; };

    matching_what = "-h";
    if( find_if(&argv[1], &argv[argc], matching) != (argv+argc) ) {
        print_usage(argv[0]);
        return 0;
    }

    try {

        matching_what = "-t";
        if( find_if( &argv[1], &argv[argc], matching) != (argv+argc) ) {
            self_test();
            cout << "Self-test succeeded!" << endl;
            return 0;
        }

        vector<int> puzzle = load_puzzle(argv[1]);
        print_puzzle( puzzle );
        solve( puzzle, print_puzzle );
        return 0;
    } catch(const char* what) {
        cout << what << endl;
        return 1;
    }
}
