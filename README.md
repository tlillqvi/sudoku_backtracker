# README #

sudoku_backtracker is a fast sudoku solver that solves sudoku puzzles using https://en.wikipedia.org/wiki/Backtracking

It finds all possible solutions for puzzles with more than 1 outcome.

### What is this repository for? ###

* sudoku solver written in C++ using a recursing/backtracking algorithm
* version 0.1.1

### How do I get set up? ###

* Tested on Debian 8, compiled using g++ (Debian 4.9.2-10) 4.9.2
* Uses C++11
* make && ./sudoku_backtracker

### Who do I talk to? ###

* Repo owner

# Examples #

## Input via bash process substitution ##

    $ ./sudoku_backtracker <(echo "
     0,5,4, 0,7,0, 0,0,0,
     0,0,0, 1,8,0, 0,0,0,
     0,0,6, 0,0,5, 0,3,9,
    
     0,0,0, 0,1,0, 0,2,0,
     0,3,0, 9,0,2, 0,0,0,
     0,7,0, 0,0,0, 6,0,4,
    
     0,2,0, 3,5,0, 0,0,0,
     0,0,0, 0,0,0, 0,0,0,
     7,0,0, 0,6,0, 0,8,0")
     -----------------
     0,5,4,0,7,0,0,0,0
     0,0,0,1,8,0,0,0,0
     0,0,6,0,0,5,0,3,9
     0,0,0,0,1,0,0,2,0
     0,3,0,9,0,2,0,0,0
     0,7,0,0,0,0,6,0,4
     0,2,0,3,5,0,0,0,0
     0,0,0,0,0,0,0,0,0
     7,0,0,0,6,0,0,8,0
     -----------------
     -----------------
     3,5,4,6,7,9,2,1,8
     2,9,7,1,8,3,4,5,6
     8,1,6,4,2,5,7,3,9
     4,8,9,7,1,6,5,2,3
     6,3,5,9,4,2,8,7,1
     1,7,2,5,3,8,6,9,4
     9,2,8,3,5,4,1,6,7
     5,6,1,8,9,7,3,4,2
     7,4,3,2,6,1,9,8,5
     -----------------
