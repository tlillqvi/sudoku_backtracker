CXXFLAGS=-g -Wall -std=c++11
CXX=g++

.PHONY: clean

.cpp.o:

sudoku_backtracker: main.o
	$(CXX) $< -o $@

clean:
	rm -f main.o
	rm -f *~
